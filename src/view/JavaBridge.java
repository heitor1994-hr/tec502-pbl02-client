package view;

import javafx.application.Platform;

/**
 * This Class works as a bridge between the JavaScript and the Java applications.
 * The methods declared here are called inside the JavaScript files.
 */
public class JavaBridge {

    /**
     * Wrapper method for WebContainer addScore
     */
    public void addScore(int score) {
        WebContainer.addScore(score);
    }

    /**
     * Wrapper method for WebContainer nextTurn
     */
    public void nextState() {
        WebContainer.nextState();
    }

    public void log(String text) {
        System.out.println(text);
    }

    public void register(String name, String password) {
        WebContainer.register(name, password);
    }

    public void login(String name, String password) {
        WebContainer.login(name, password);
    }

    public void selectRoom(int gameOption) {

        WebContainer.selectRoom(gameOption);
    }

    public void selectServer(String ip) {
        WebContainer.selectServer(ip);
    }

    public void getPlayersRank() {
        WebContainer.getPlayersRank();
    }

    public void broadcastRank() {
        WebContainer.broadcastRank();
    }

    public int getRunningTotal() {
        return WebContainer.getRunningTotal();
    }

    public void broadcastRouletteValue() {
        WebContainer.broadcastRouletteValue();
    }

    public void broadcastLetter(char letter) {
        WebContainer.broadcastLetter(letter);
    }

    public void broadcastPass() {
        WebContainer.broadcastPass();
    }

    public String getReceivedData() {
        return WebContainer.getReceivedData();
    }

    public void broadcastNextRound() {
        WebContainer.broadcastNextRound();
    }

    public boolean hasNewMessage() {
        return WebContainer.hasNewMessage();
    }

    /**
     * Wrapper method for WebContainer reset
     */
    public void reset() {
        WebContainer.reset();
    }

    /**
     * Closes the whole application.
     */
    public void exit() {
        Platform.exit();
    }

    public boolean isReady() {
        return WebContainer.isReady();
    }

    public boolean readResponse() {
        return WebContainer.readResponse();
    }

    public void addRunningTotal(int v1, int v2, int v3) {
        WebContainer.addRunningTotal(v1, v2, v3);
    }

}
