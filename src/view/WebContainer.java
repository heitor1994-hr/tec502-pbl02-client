package view;


import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;


import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import model.GameController;
import netscape.javascript.JSObject;


/**This Class encapsulates the View of this application. It creates a container
 * like a Web Browser so that the interface can make use of HTML, CSS and JavaScript.*/
public class WebContainer extends Application {

     final private JavaBridge bridge = new JavaBridge();
	 /**Scene to display the Web View*/
	 private Scene scene;
	 /**Reference to the Web View engine*/
	 private static WebEngine we;
	 
	 private static JSObject window;

	 /**Instance of GameController for game control*/
	 private static GameController gameController = new GameController();
	 
	 	/** Starts a stage with scene (object that creates the window where the browser is going to display it's content) 
	 	 * and Web View (Browser object that reads HTML and CSS, and encapsulates the JavaScript 
	 	 * interpreter) in it.
	 	 * @param stage		The stage that's going to hold the web content*/
	    @Override 
	    public void start(Stage stage) {
	        // create the scene
	        stage.setTitle("Web View");
	        
	        //Create new Web View
	        Browser browser = new Browser();
	        
	        //Hold reference for the Web View's engine for JavaScript interaction
	        we = browser.webEngine;
	        
	        //Variable to get users screen dimensions
	        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

	        //Create a new scene for the Web View
	        scene = new Scene(browser,screenSize.getWidth(),screenSize.getHeight()-20, Color.web("#666970"));

	        //Set the scene to the stage
	        stage.setScene(scene);

	        //Application has predefined dimensions and it's not resizable to avoid bugs in the View
	        stage.setResizable(false);

	        //Start rendering
	        stage.show();

	        //Waits for the page to be fully loaded, and executes a JavaScript expression
	        we.getLoadWorker().stateProperty().addListener(
	        		 new ChangeListener<State>() {
	        		        public void changed(@SuppressWarnings("rawtypes") ObservableValue ov, State oldState, State newState) {
	        		            if (newState == Worker.State.SUCCEEDED) {
	        		            	window = (JSObject) we.executeScript("window");
	        		            	window.setMember("app", bridge);
	        		             	executeJavaScript(gameController.jsExpression());
	        		            }
	        		        }
	        		    });
	    }
	    
	    //Launch application
	    public static void main(String[] args){
	        launch(args);
	    }
	    
	    public static void executeJavaScript(String expression) {
			//JSObject win = (JSObject) we.executeScript("window");
	
			// Create a variable in JavaScript that references a JavaBridge Object
			// in the application model
			
	
			// Everytime the page is loaded, data is sent to the Server
			// and the response is used as a JavaScript expression
			// to set the Web View
			we.setJavaScriptEnabled(true);
			we.executeScript(expression);
	    }
	    
	    /**Method that sets the GameModel's score as the score earned in the current turn
	     * @param nscore		Total score of the current turn*/
	    public static void addScore (int nscore) {
	    	gameController.addScore(nscore);
	    }
	    
	    /**Method that makes the GameModel increment its current turn and reload the Web View*/
	    public static void nextState () {
	    	gameController.nextState();
	    	executeJavaScript(gameController.jsExpression());
	    }
	    
	    /**Method responsible for resetting GameModel state and reloading the Web View*/
	    public static void reset () {
	    	gameController.reset();
	    	we.reload();
	    }
	    
	    public static void register(String name, String password) {
	    	gameController.register(name, password);
	    }
	    
	    public static void login(String name, String password) {
	    	gameController.login(name, password);
	    }
	    
	    public static void selectRoom(int gameOption){
	    	
	    	gameController.selectRoom(gameOption);	    
	    }

	    public static void selectServer(String ip) {
	    	gameController.selectServer(ip);
		}
	    
	    public static boolean readResponse() {
	    	boolean OK = gameController.readResponse();
	    	if (OK) nextState();
	    	return OK;
	    }
		public static void getPlayersRank() {
			gameController.getPlayersRank();
		}
		
		public static String getReceivedData() {
			return gameController.getReceivedData();
		}
		
		public static boolean hasNewMessage() {
			return gameController.hasNewMessage();
		}

		public static void broadcastRouletteValue() {
			gameController.broadcastRouletteValue();
		}

		public static void broadcastLetter(char letter) {
			gameController.broadcastLetter(letter);
		}

		public static void broadcastPass() {
			gameController.broadcastPass();
		}
		
		public static void broadcastRank() {
			gameController.broadcastRank();
		}
		
		public static int getRunningTotal() {
			return gameController.getRunningTotal();
		}

		public static boolean isReady() {
			return gameController.isReady();
		}
		
		public static void broadcastNextRound() {
			gameController.broadcastNextRound();
		}
		
		public static void addRunningTotal(int v1, int v2, int v3) {
			gameController.addRunningTotal(v1,v2,v3);
		}

}


/**This class represents the Web View, where HTML and CSS is going to render and JavaScript will be interpreted.*/
class Browser extends Region{
	
	/** Create new Web View*/
	final WebView browser = new WebView();
	/**Get Web View engine.*/
    final WebEngine webEngine = browser.getEngine();
    /**Absolute path to Web files*/
    private String absPath;
     
    public Browser() {
    	
    	//Get path to the web files
    	Path cp = Paths.get("");
		this.absPath = cp.toAbsolutePath().toString();
		
		String sysName = System.getProperty("os.name");
		if (sysName.contains("Windows")) this.absPath += "\\";
		else this.absPath += "/";
		
		this.absPath += "www";

		//Disabled right-click inside Web View
		browser.setContextMenuEnabled(false);
		
		//Load the index.html file as the main layout
		File indexFile = new File(absPath+"/index.html");
        try {
			webEngine.load(indexFile.toURI().toURL().toString());
		} catch (MalformedURLException e) {
			System.err.println("Bad URL: "+e.getMessage());
		}
        
        //Debugging line
        webEngine.setOnAlert(event -> showAlert(event.getData()));

        //Add the web view to the scene
        getChildren().add(browser);
 
    }

    /**Debugging method*/
    private void showAlert(String message) {
        Dialog<Void> alert = new Dialog<>();
        alert.getDialogPane().setContentText(message);
        alert.getDialogPane().getButtonTypes().add(ButtonType.OK);
        alert.showAndWait();
    }

    
    /**Browser's children layout*/
    @Override protected void layoutChildren() {
        double w = getWidth();
        double h = getHeight();
        layoutInArea(browser,0,0,w,h,0, HPos.CENTER, VPos.CENTER);
    }
 

    @Override protected double computePrefWidth(double height) {
        return 1000;
    }
 
    @Override protected double computePrefHeight(double width) {
        return 800;
    }
    
}