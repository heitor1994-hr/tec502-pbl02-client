package networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ProcessString implements Runnable {

    private String message;
    private Response res;
    private String host;
    private int port;

    public ProcessString(String msg) {
        message = msg;
    }

    public void run() {
        //try-with-resources statement for opening streams and automatically closing it.
        Socket socket = null;
        PrintWriter pw;
        BufferedReader in;
        try {
            socket = new Socket(host, port);
            pw = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String line;
            String[] varInput;

            pw.println(message); // Send a request to the server

            while ((line = in.readLine()) != null) {
                varInput = line.split("=");
                if (varInput[0].equals("WORD")) {
                    res.setWord(varInput[1]);
                } else if (varInput[0].equals("HINT")) {
                    res.setHint(varInput[1]);
                } else if (varInput[0].equals("IND")) {
                    res.setWordInd(Integer.parseInt(varInput[1]));
                } else if (varInput[0].equals("FINAL_SCORE")) {
                    res.setFinalScore(Integer.parseInt(varInput[1]));
                    res.setOK(true);
                } else if (varInput[0].equals("FIRST") || varInput[0].equals("SECOND")
                        || varInput[0].equals("THIRD")) {
                    String[] nameScore = varInput[1].split("&");
                    res.addPlayerRank(nameScore[0], Integer.parseInt(varInput[2]));
                } else if (varInput[0].equals("RECORD")) {
                    res.setBestScore(Integer.parseInt(varInput[1]));
                } else if (varInput[0].equals("OK")) {
                    res.setOK(true);
                } else if (varInput[0].equals("FAIL")) {
                    res.setOK(false);
                } else if (varInput[0].equals("GAME_SET")) {
                    res.setGameSet(true);
                    res.setOK(true);
                } else if (varInput[0].equals("WORDS")) {
                    String[] word = varInput[1].split("&");
                    res.addWordAndHint(word[0], varInput[2]);
                } else if (varInput[0].equals("MC_ADDR")) {
                    res.setMulticastAddress(varInput[1]);
                } else if (varInput[0].equals("TURN")) {
                    res.setPlayerTurn(varInput[1]);
                } else if (varInput[0].equals("NAMES")) {
                    String[] names = varInput[1].split(";");
                    for (int i = 0; i < names.length; i++) {
                        res.addLogin(names[i]);
                    }
                } else
                    break;
            }
            synchronized (res) {
                res.setReady(true);
                GameConnection.setLastResponse(res);
            }
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setResponse(Response res) {
        this.res = res;
    }

    public void setHostAndPort(String host, int port) {
        this.host = host;
        this.port = port;
    }
}
