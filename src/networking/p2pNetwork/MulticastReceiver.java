package networking.p2pNetwork;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;

/**
 * Receives data from other peers
 * */
public class MulticastReceiver implements Runnable{
	/**Socket to send ack and receive data*/
	private MulticastSocket socket;
	private final int BUFFER_SIZE = 512;
		
	public MulticastReceiver () throws IOException {
		socket = new MulticastSocket(LogicNetwork.RECEIVE_PORT);
		socket.joinGroup(LogicNetwork.getGroup());
		
	}
	
	public void run() {
		byte[] buffer;
		
		boolean in = true;
		
		while (in) {
			try {
				buffer = new byte[BUFFER_SIZE];
				DatagramPacket pkt =  new DatagramPacket(buffer,buffer.length);
				
				//Stop this thread until data is available
				socket.receive(pkt);
				
				System.out.println("[<==] Received (Sock Addr: "+ socket.getLocalSocketAddress() +" "
						+ "Pack Addr: "+ pkt.getAddress()+":"+pkt.getPort() +" ): "+new String(pkt.getData(),"UTF-8"));
				
				//If it is an Ack or this peer is sending data, drop the packet
				if(new String(pkt.getData(),"UTF-8").contains("ACK")) continue;
				
				String[] data = getPacketData(pkt);
				
				int pktVal = getPktCount(data[1]);
				int peerId = getPktPeerId(data[0]);
				
				if (peerId == LogicNetwork.peerId) continue;
				
				sendAck();
				
				//If it isn't the packet expected, drop it
				if (pktVal != LogicNetwork.getPack_cnt()) continue;
				
				LogicNetwork.incPackCnt();
				
				LogicNetwork.feedData(data[2]);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private int getPktPeerId(String data) {
		String[] varInput = data.split(":");
		return Integer.parseInt(varInput[1]);
	}

	public void sendAck () {
		String ack = new String("ACK");
		byte[] buf = ack.getBytes();
		DatagramPacket pkt = new DatagramPacket(buf,buf.length, LogicNetwork.getGroup(), LogicNetwork.SENDING_PORT);
		
		try {
			socket.send(pkt);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public int getPktCount (String data){
		String[] varInput = data.split(":");
		return Integer.parseInt(varInput[1]);
	}
		
	public String[] getPacketData (DatagramPacket pkt) throws UnsupportedEncodingException {
		byte[] buf = pkt.getData();
		
		String pktString = new String (buf,"UTF-8");
		
		StringBuilder res = new StringBuilder(BUFFER_SIZE);
		
		for (int i = 0; i < buf.length; i++) {
			if (isZero(buf[i])) break;
			res.append(pktString.charAt(i));
		}
		String[] cntString = res.toString().split("-");
		
		return cntString;
	}
	
	public boolean isZero (byte bt) {
		return (bt==0);
	}
	
	public void closeSocket() throws IOException {
		socket.leaveGroup(socket.getInetAddress());
		socket.close();
	}
	
	public int getBufferSize() {
		return BUFFER_SIZE;
	}
}
