package networking.p2pNetwork;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that encapsulates the data exchanged between clients
 * */
public class BroadcastData {
	
	private int playerTurn;
	private char letter;
	private int rouletteValue;
	private boolean isPass;
	private boolean isAck;
	private boolean isNextRound;
	
	private MessageType type;
	
	/**Names and scores of the players in the Rank*/
	private List<String> rank_names;
	private List<Integer> rank_scores;
	
	public enum MessageType {
		ROULETTE, PLAYED_LETTER, PASS, ACK, NEXT_ROUND, RANK
	}
	
	public BroadcastData () {
		playerTurn = 0;
		letter = '#';
		rouletteValue = -1;
		isPass = false;
		setNextRound(false);
		isAck = false;
		rank_names = new ArrayList<>();
		rank_scores = new ArrayList<>();
	}
	
	public int getPlayerTurn() {
		return playerTurn;
	}
	public void setPlayerTurn(int playerTurn) {
		this.playerTurn = playerTurn;
	}
	public char getLetter() {
		return letter;
	}
	public void setLetter(char letter) {
		this.letter = letter;
	}
	public int getRouletteValue() {
		return rouletteValue;
	}
	public void setRouletteValue(int rouletteValue) {
		this.rouletteValue = rouletteValue;
	}

	public boolean isPass() {
		return isPass;
	}

	public void setPass(boolean isPass) {
		this.isPass = isPass;
	}

	public boolean isAck() {
		return isAck;
	}

	public void setAck(boolean isAck) {
		this.isAck = isAck;
	}

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public boolean isNextRound() {
		return isNextRound;
	}

	public void setNextRound(boolean isNextRound) {
		this.isNextRound = isNextRound;
	}
	
	public void addRankName (String name) {
		rank_names.add(name);
	}
	
	public void addRankScore(int score) {
		rank_scores.add(score);
	}
	
	public String getRankName (int ind) {
		return rank_names.get(ind);
	}
	
	public int getRankScore (int ind) {
		return rank_scores.get(ind);
	}
	
}
