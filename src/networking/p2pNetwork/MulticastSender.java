package networking.p2pNetwork;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.ConcurrentLinkedQueue;


/**
 * Sends data through the network to other peers
 */
public class MulticastSender implements Runnable {
    /**
     * Socket that is going to received acks
     */
    private MulticastSocket socket;
    /**
     * Queue of data to be sent
     */
    private static ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<String>();

    public MulticastSender() throws IOException {
        socket = new MulticastSocket(LogicNetwork.SENDING_PORT);
        socket.joinGroup(LogicNetwork.getGroup());
    }

    public void closeSocket() throws IOException {
        socket.close();
    }

    public void sendData(String data) throws IOException {

        //data to be sent with packet counter and peer ID
        data = new String("ID:" + LogicNetwork.peerId + "-CNT:" + LogicNetwork.getPack_cnt() + "-" + data);

        //data to be sent in bytes
        byte[] buffer = data.getBytes();

        DatagramPacket pkt = new DatagramPacket(buffer, buffer.length, LogicNetwork.getGroup(), LogicNetwork.RECEIVE_PORT);

        //finally, send the data
        socket.send(pkt);

    }

    /**
     * Add data to the queue
     */
    public synchronized void addToQueue(String data) {
        queue.add(data);
    }

    @Override
    public void run() {
        boolean in = true;
        while (in) {
            while (!queue.isEmpty()) {

                String data = queue.poll();

                try {
                    sendData(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                System.out.println("[==>] Sending (Sock addr: " + socket.getLocalAddress() + "): " + data);

                byte[] buffer = new byte[512];

                DatagramPacket pkt = new DatagramPacket(buffer, buffer.length);

                //How many acks have to be received
                int repAcks = LogicNetwork.getAcks();
                try {
                    //Wait 5 second and resend information
                    socket.setSoTimeout(5000);
                } catch (SocketException e1) {
                    e1.printStackTrace();
                }
                while (repAcks > 0) {
                    try {
                        //Received Ack
                        socket.receive(pkt);

                        String recData = new String(pkt.getData());

                        if (recData.contains("ACK")) {
                            repAcks--;
                            System.out.println("[ ! ] ACK Received, Left: " + repAcks);
                        }

                    } catch (SocketTimeoutException s) {
                        try {
                            //In case of timeout, resend data
                            sendData(data);
                            repAcks = LogicNetwork.getAcks();
                            System.out.println("[==>] Resending: " + data);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                LogicNetwork.incPackCnt();
            }
        }
    }
}
