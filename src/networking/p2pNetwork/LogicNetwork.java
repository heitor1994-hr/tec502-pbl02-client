package networking.p2pNetwork;


import java.io.IOException;
import java.net.InetAddress;
import java.util.concurrent.ConcurrentLinkedQueue;

import networking.p2pNetwork.BroadcastData.MessageType;


/** Controls attributes and methods for the peer-to-peer network*/
public class LogicNetwork {
	
	public static final int RECEIVE_PORT = 4446;
	public static final int SENDING_PORT = 4444;
	public static final String NO_NEW_MESSAGE = "NO MESSAGE";
	public static int peerId;
	
	private static ConcurrentLinkedQueue<String> dataQueue = null;
	private static InetAddress group = null;
	
	private static MulticastSender sender = null;
	private static MulticastReceiver receiver = null;
	
	private static Thread receiverThread = null;
		
	private static int acks;
	
	private static int pack_cnt;
	
	public LogicNetwork () throws IOException {
		dataQueue = new ConcurrentLinkedQueue<String>();
		pack_cnt = 1;
		peerId = 0;
	}
	
	public void startLogicNetwork (String groupAddr) throws IOException {
		group = InetAddress.getByName(groupAddr);
		
		receiver = new MulticastReceiver();
		sender = new MulticastSender();
		new Thread(sender).start();
	}
	
	public void startReceiving() throws IOException {
		receiverThread = new Thread(receiver, "receiver");
		receiverThread.start();
	}
	
	public void stopReceiving() throws IOException {
		receiver.closeSocket();
	}
	
	public void sendData(String data) throws IOException {
		sender.addToQueue(data);
	}
	
	public static void feedData (String data) {
		dataQueue.add(data);
	}
	
	public static InetAddress getGroup (){
		return group;
	}
	
	public synchronized BroadcastData getNextMessage() {
		if (hasNewMessage()) {
			BroadcastData bd = null;
			String data = dataQueue.poll();
			
			String[] lines = data.split("\r\n");
			
			bd = processData(lines);
			
			return bd;
		}
		return null;
	}
	
	public synchronized boolean hasNewMessage() {
		return !dataQueue.isEmpty();
	}
	
	public BroadcastData processData(String[] lines) {
		BroadcastData bd = new BroadcastData();
		for (String s: lines) {
			String[] varInput = s.split("=");
			if (varInput[0].equals(MessageType.ROULETTE.name())) {
				bd.setType(MessageType.ROULETTE);
			} else if (varInput[0].equals(MessageType.PLAYED_LETTER.name())) {
				bd.setType(MessageType.PLAYED_LETTER);
			} else if (varInput[0].equals(MessageType.PASS.name())) {
				bd.setType(MessageType.PASS);
				bd.setPass(true);
			} else if (varInput[0].equals(MessageType.NEXT_ROUND.name())) {
				bd.setType(MessageType.NEXT_ROUND);
				bd.setNextRound(true);
			} else if (varInput[0].equals(MessageType.RANK.name())) {
				bd.setType(MessageType.RANK);
			} else if (varInput[0].equals("PLAYER")) {
				int player = Integer.parseInt(varInput[1]);
				bd.setPlayerTurn(player);
			} else if (varInput[0].equals("VALUE")) {
				int value = Integer.parseInt(varInput[1]);
				bd.setRouletteValue(value);
			} else if (varInput[0].equals("LETTER")) {
				char letter = varInput[1].charAt(0);
				bd.setLetter(letter);
			} else if (varInput[0].equals("FIRST") || varInput[0].equals("SECOND") || varInput[0].equals("THIRD")) {
				String[] nameScore = varInput[1].split("&");
				bd.addRankName(nameScore[0]);
				bd.addRankScore(Integer.parseInt(varInput[2]));
			}
		}
		return bd;
	}
	
	public String createRouletteBroadcastString(int player, int value) {
		String req = "ROULETTE\r\n";
		req += "PLAYER="+player+"\r\n";
		req += "VALUE="+value+"\r\n";
		return req;
	}
	
	public String createLetterBroadcastString(int player, char letter) {
		String req = "PLAYED_LETTER\r\n";
		req += "PLAYER="+player+"\r\n";
		req += "LETTER="+letter+"\r\n";
		return req;
	}
	
	public String createPassBroadcastString(int player) {
		String req = "PASS\r\n";
		req += "PLAYER="+player+"\r\n";
		return req;
	}
	
	public String createNextRoundBroadcast (int player) {
		String req = "NEXT_ROUND\r\n";
		req += "PLAYER="+player+"\r\n";
		return req;
	}
	
	public String createRankBroadcast (String name1, String name2, String name3, int score1, int score2, int score3) {
		String req = "RANK\r\n";
		req += "FIRST="+name1+"&SCORE="+score1+"\r\n";
		req += "SECOND="+name2+"&SCORE="+score2+"\r\n";
		req += "THIRD="+name3+"&SCORE="+score3+"\r\n";
		return req;
	}

	public static int getAcks() {
		return acks;
	}

	public static void setAcks(int a) {
		acks = a;
	}

	public static int getPack_cnt() {
		return pack_cnt;
	}

	public static void incPackCnt() {
		pack_cnt++;
	}
	
	public void setPeerId (int id) {
		peerId = id;
	}
}
