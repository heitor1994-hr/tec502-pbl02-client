package networking;

public class ServerInfo {

    private String ip;
    private String name;
    private long latency;

    public ServerInfo (String name, String ip, long latency) {
        this.name = name;
        this.ip = ip;
        this.latency = latency;
    }

    public String getIp() {
        return ip;
    }

    public String getName() {
        return name;
    }

    public long getLatency() {
        return latency;
    }
}
