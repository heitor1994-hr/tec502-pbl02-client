package networking;

import java.util.ArrayList;
import java.util.List;

/**This Class holds all information received from the Server to be used as needed.
 * The Class is composed only by getters and setters, no important computing is done here.*/
public class Response {
	
	/**Word received from the Server*/
	private String word;
	/**Hint received from the Server*/
	private String hint;
	
	/**Index of the last word received from the Server*/
	private Integer wordInd;

	/**Final Score received from the Server*/
	private Integer finalScore;
	/**Player Best score received from the Server*/
	private Integer bestScore;

	/**Names and scores of the players in the Rank*/
	private List<String> rank_names;
	private List<Integer> rank_scores;
	
	/**Check if requested operation was successful*/
	private boolean OK;
	
	/**Check whether the game is ready to begin or not */
	private boolean gameSet;
	
	private boolean ready;
	
	private List<String> words;
	private List<String> hints;
	private List<String> playersLogin;
	private String multicastAddr;
	private int playerTurn;
	
	
	public Response () {
		word = null;
		hint = null;
		wordInd = null;
		rank_names = new ArrayList<>();
		rank_scores = new ArrayList<>();
		playersLogin = new ArrayList<>();
		words = new ArrayList<>();
		hints = new ArrayList<>();
		multicastAddr = null;
		playerTurn = -1;
		OK = false;
		bestScore = -1;
		ready = false;
	}
	
	public Integer getBestScore() {
		return bestScore;
	}

	public void setBestScore(Integer bestScore) {
		this.bestScore = bestScore;
	}

	public Integer getWordInd() {
		return wordInd;
	}

	public void setWordInd(Integer wordInd) {
		this.wordInd = wordInd;
	}

	public void setWord (String word) {
		this.word = word;
	}
	
	public void setHint (String hint) {
		this.hint = hint;
	}

	public String getWord() {
		return word;
	}
	
	public String getWord(int index) {
		return words.get(index);
	}

	public String getHint() {
		return hint;
	}
	
	public String getHint(int index){
		return hints.get(index);
	}
	
	public String getMulticastAddr() {
		return multicastAddr;
	}

	public int getPlayerTurn() {
		return playerTurn;
	}

	public void setFinalScore(int score) {
		finalScore = score;
	}
	public int getFinalScore() {
		return finalScore;
	}
	
	public void addPlayerRank(String name, int score) {
		rank_names.add(name);
		rank_scores.add(score);
	}
	
	public String getPlayerName (int index) {
		return rank_names.get(index);
	}
	
	public Integer getPlayerScore (int index) {
		return rank_scores.get(index);
	}
	
	public boolean getOK () {
		return this.OK;
	}
	public void setOK (boolean OK) {
		this.OK = OK;
	}

	public boolean isGameSet() {
		return gameSet;
	}

	public void setGameSet(boolean gameSet) {
		this.gameSet = gameSet;
	}
	
	public void addWordAndHint (String word,String hint) {
		this.words.add(word);
		this.hints.add(hint);
	}
	
	public void addLogin (String name) {
		this.playersLogin.add(name);
	}
	
	public String getPlayerLogin(int ind) {
		return playersLogin.get(ind);
	}
	
	public void setMulticastAddress (String addr){
		this.multicastAddr = addr;
	}
	
	public void setPlayerTurn (String turn) {
		this.playerTurn = Integer.parseInt(turn);
	}
	
	public boolean hasWordsAndHints(){
		return (!words.isEmpty() && !hints.isEmpty());
	}
	
	public boolean hasPlayersRank() {
		return (!rank_names.isEmpty() && !rank_scores.isEmpty());
	}
	
	public boolean hasBestScore() {
		return (bestScore != -1);
	}
	
	public boolean hasPlayerTurn() {
		return (playerTurn != -1);
	}
	
	public boolean hasPlayersLogin() {
		return (!playersLogin.isEmpty());
	}
	
	public boolean isReady() {
		return ready;
	}
	
	public void setReady(boolean val) {
		ready = val;
	}
}
