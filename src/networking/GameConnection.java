package networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

/**
 * This Class is responsible for knowing the Server address and sending and receiving data through the network.
 */
public class GameConnection {

    /**
     * Server IP address
     */
    private String host;
    /**
     * The port where the Server is running
     */
    private int port;

    /**
     * Reference to itself (Singleton pattern)
     */
    private static GameConnection gc = null;

    /**
     * Reference to MessageDigest object for password hashing
     */
    private static MessageDigest md = null;
    /**
     * Salt added to hash as a way to avoid easy password cracking
     */
    private static String salt = "mysalt";

    private static Response lastResponse;

    public static List<ServerInfo> servers = null;
    private static int BALANCER_PORT = 9000;

    /**
     * Sets the port to 8080 and asks for the Server IP
     */
    private GameConnection() {
        servers = new ArrayList<>();
        lastResponse = new Response();
        boolean bad_host = true;

        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        //Ask for Server IP while it is not a valid IP Address.
        while (bad_host) {
            host = JOptionPane.showInputDialog("Balancer IP");
            if (host == null) System.exit(0);
            if (!checkHost(host)) {
                Object[] options = {"OK"};
                JOptionPane.showOptionDialog(null,
                        "Insira um endereço válido.", "ERRO",
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE,
                        null,
                        options,
                        options[0]);
                bad_host = true;
            } else bad_host = false;
        }

        try {
            Socket balancerSocket = new Socket(host, BALANCER_PORT);

            BufferedReader br = new BufferedReader(new InputStreamReader(balancerSocket.getInputStream()));

            String firstLine = br.readLine();
            int servers_len = Integer.valueOf(firstLine.split("=")[1]);

            for (int i = 0; i < servers_len; i++) {
                String serverName = br.readLine().split("=")[1];
                String serverIp = br.readLine().split("=")[1];
                long latency = 0;

                long start = System.currentTimeMillis();
                boolean isReachable = InetAddress.getByName(serverIp).isReachable(2000);
                long end = System.currentTimeMillis();
                latency = end - start;

                if (isReachable) {
                    servers.add(new ServerInfo(serverName, serverIp, latency));
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }


        port = 8080;
    }

    /**
     * Get a unique reference to this class (Singleton pattern)
     */
    public static GameConnection getInstance() {
        if (gc == null) gc = new GameConnection();
        return gc;
    }

    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Method for checking if the Server IP entered is valid.
     *
     * @param host Server IP
     * @return true if valid, false otherwise
     */
    private boolean checkHost(String host) {
        if (host.equals("localhost")) return true;
        if (host.equals("")) return false;
        String[] vals = host.split("\\.");
        if (vals.length != 4) return false;
        for (int i = 0; i < 4; i++) {
            if (!isNumeric(vals[i])) return false;
            int v = Integer.parseInt(vals[i]);
            if (v < 0 || v > 255) return false;
        }
        return true;
    }

    /**
     * Auxiliary method for checking if a string is numeric.
     *
     * @param str String to be checked
     * @return true if numeric, false otherwise
     */
    private static boolean isNumeric(String str) {
        try {
            @SuppressWarnings("unused")
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * This methods sends data through a Socket and gets the response from the Server.
     * The data sent is a String following the rules of the GameProtocol.
     *
     * @param msg Message to be sent
     * @return Response object with the data received.
     * @throws UnknownHostException Exception in case of unknown host
     * @throws IOException          Exception in case it can't send or receive data from the Server
     */
    public void sendString(String msg) throws UnknownHostException, IOException {
        lastResponse = new Response();

        ProcessString ps = new ProcessString(msg);
        ps.setResponse(lastResponse);
        ps.setHostAndPort(host, port);
        Thread th = new Thread(ps);
        th.setDaemon(true);
        th.start();
    }


    /**
     * Creates a String asking the Server for the players Rank following the GameProtocol
     *
     * @param name  The name of the player
     * @param score Score of the turn that ended
     * @return the String corresponding for a Rank request following the GameProtocol
     */
    public String createRankRequest(String name, int score) {
        String req = "GET_RANK\r\n";
        req += "NAME=" + name + "\r\n";
        req += "SCORE=" + score + "\r\n";
        return req;
    }

    /**
     * Creates a String asking the Server for a new word following the GameProtocol
     *
     * @param level    Current turn
     * @param prev_ind List of previous words indexes
     * @param score    Score of the turn that ended
     * @param name     The name of the player
     * @return the String corresponding for a Word request following the GameProtocol
     */
    public String createWordRequest(int level, List<Integer> prev_ind, int score, String name) {
        String req = "GET_WORD\r\n";
        req += "NAME=" + name + "\r\n";
        req += "LEVEL=" + String.valueOf(level) + "\r\n";
        req += "PREV_WORD_IND=";
        boolean first = true;
        for (int v : prev_ind) {
            if (first) {
                req += String.valueOf(v);
                first = false;
            } else req += ";" + String.valueOf(v);
        }
        req += "\r\nSCORE=" + String.valueOf(score) + "\r\n";
        return req;
    }

    /**
     * Creates a String asking the Server to register a player, following the GameProtocol
     * The password is turned into MD5 hash (128-bit hash value)
     *
     * @param name     The name of the player
     * @param password Player's password
     * @return the String corresponding for a Register request following the GameProtocol
     */
    public String createRegisterRequest(String name, String password) {
        String req = "REGISTER\r\n";
        req += "NAME=" + name + "\r\n";

        byte[] passHash = md.digest((password + salt).getBytes());
        String pass = new String(passHash, StandardCharsets.UTF_8);
        req += "PASSWORD=" + pass + "\r\n";
        return req;
    }


    /**
     * Creates a String asking the Server to login a player, following the GameProtocol
     * The password is turned into MD5 hash (128-bit hash value)
     *
     * @param name     The name of the player
     * @param password Player's password
     * @return the String corresponding for a Login request following the GameProtocol
     */
    public String createLoginRequest(String name, String password) {
        String req = "LOGIN\r\n";
        req += "NAME=" + name + "\r\n";

        byte[] passHash = md.digest((password + salt).getBytes());
        String pass = new String(passHash, StandardCharsets.UTF_8);
        req += "PASSWORD=" + pass + "\r\n";
        return req;
    }

    public String join2PlayersGame(String login) {
        String req = "JOIN2PLAYERS\r\n";
        req += login + "\r\n";
        return req;
    }

    public String join3PlayersGame(String login) {
        String req = "JOIN3PLAYERS\r\n";
        req += login + "\r\n";
        return req;
    }


    public synchronized boolean isReady() {
        return lastResponse.isReady();
    }

    public Response getLastResponse() {
        lastResponse.setReady(false);
        return lastResponse;
    }

    public synchronized static void setLastResponse(Response res) {
        lastResponse = res;
    }
}
