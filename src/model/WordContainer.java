package model;

public class WordContainer {
	
	private String[] words;
	private String[] hints;
	
	public WordContainer(String[] newWords, String[] newHints) {
		words = newWords;
		hints = newHints;
	}
	
	
	public String getWord(int index) {
		return words[index];
	}
	
	public String getHint(int index){
		return hints[index];
	}
	
	public boolean letterInWord(int ind, char c) {
		String word = words[ind].toUpperCase();
		for (int i = 0; i < word.length(); i++) {
			if (word.charAt(i) == c) return true;
		}
		return false;
	}
}	

	
