package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import networking.GameConnection;
import networking.Response;
import networking.ServerInfo;

/**
 * This Class takes care of all the game attributes and how the game communicates with the Server.
 * */
public class GameModel {
	
	/** Score of this turn */
	private int score;
	/**Player login*/
	private String login;
	/**Running total*/
	private Integer[] runningTotal;
	
	private GameState currentState;
	private int manyPlayers;
	
	/** Instance of GameModel (Singleton pattern) */
	private static GameModel gm = null;
	
	private WordContainer wc;
	
	private int playerTurn;
	private int gameTurn;
	
	private List<String> playersLogin;
	private List<String> rankNames;
	private List<Integer> rankScores;
	private int playerBestScore;
	
	private String winnerLogin;
	private int winnerScore;
	
	
	public enum RoomType {
		TWO_PLAYERS, THREE_PLAYERS
	}
	
	public GameModel() {
		startGame();
	}
	
	public int getGameTurn() {
		return gameTurn;
	}

	public void setGameTurn(int gameTurn) {
		this.gameTurn = gameTurn;
	}

	/** Get instance of GameModel (Singleton pattern)*/
	public static GameModel getInstance() {
		if (gm == null) gm = new GameModel();
		return gm;
	}
	
	public List<ScoreBoardItem> createScoreBoardList () {
		List<ScoreBoardItem> list = new ArrayList<>();
		list.add(new ScoreBoardItem(playersLogin.get(0), runningTotal[0]));
		list.add(new ScoreBoardItem(playersLogin.get(1), runningTotal[1]));
		if (manyPlayers > 2) list.add(new ScoreBoardItem(playersLogin.get(2), runningTotal[2]));
		
		Collections.sort(list,Collections.reverseOrder());
		
		setWinnerLogin(list.get(0).getPlayerLogin());
		setWinnerScore(list.get(0).getTotal());
		
		return list;
	}
	
	public String generateJSExpression() {
		StringBuilder exp = new StringBuilder();

		// use console.log
		exp.append("console.log = function(message) { app.log(message); }; ");
		
		if(currentState.inGame()) {
			if (currentState == GameState.ROUND_1) {
				exp.append(" hideModals(); ");
				exp.append(" setPlayerName('"+this.login+"'); ");
				exp.append(" setScoreBoard("+(this.manyPlayers-2)+"); ");
				exp.append(" setPlayerTurn("+this.playerTurn+"); ");
				exp.append(" setGameTurn("+this.gameTurn+"); ");
				for (int i = 0; i < manyPlayers; i++) exp.append(" setName("+(i+1)+",'"+playersLogin.get(i)+"'); ");
			}
			
			exp.append(" setRound(" + (currentState.getVal()+1) + "); ");
			exp.append(" setCurrentWord('"+wc.getWord(currentState.getVal())+"');  ");
			exp.append(" setHint('" + wc.getHint(currentState.getVal()) + "'); ");

		} else if (currentState == GameState.SELECT_SERVER) {
		    exp.append(" showServerModal([");

		    for (ServerInfo si : GameConnection.servers) {
		        exp.append(" { ");
		        exp.append(" \"name\": \""+si.getName()+"\",");
		        exp.append(" \"ip\": \""+si.getIp()+"\",");
		        exp.append(" \"latency\": "+si.getLatency());
		        exp.append("},");
            }
		    exp.append("]); ");

		} else if (currentState == GameState.LOGIN) {
			
			exp.append(" showIniModal(); ");
			
		} else if (currentState == GameState.ROOM_SELECTION) {
			
			exp.append(" showRoomModal(); ");
			
		} else if (currentState == GameState.GAME_SCORE) {
			List<ScoreBoardItem> list = createScoreBoardList();
			
			for (int i = 0; i < list.size(); i++) {
				exp.append(" addToScoreBoard("+(i+1)+", '"+list.get(i).getPlayerLogin()+"',"+list.get(i).getTotal()+" );" );
			}
			if (list.size() < 3) exp.append(" hideThirdPlace(); ");
			
			exp.append(" showPartidaModal(); ");
			
		} else if (currentState  == GameState.GAME_END) {
			
			 exp.append(" setTotalScore(" + runningTotal[playerTurn-1] + "); ");
			 for (int i = 0; i < 3; i++)
        		 exp.append(" setPlayerRank(" + i + ",'" + rankNames.get(i) + "'," + rankScores.get(i) + "); ");
			 exp.append(" setBestScore(" + playerBestScore + "); ");
			 exp.append(" showModal(); ");
			 
		}
		return exp.toString();
	}
	
	/**Clear the score for the current turn.*/
	public void clearScore () {
		score = 0;
	}
	
	public void clearRunningTotal() {
		runningTotal[0] = 0;
		runningTotal[1] = 0;
		runningTotal[2] = 0;
	}
	
	/**Reinitialize variables for a new game.*/
	public void startGame() {
		score = 0;
		playersLogin = new ArrayList<>();
		login = null;
		wc = null;
		gameTurn = 1;
		
		manyPlayers = 1;
		currentState = GameState.SELECT_SERVER;
		rankNames = new ArrayList<>();
		rankScores = new ArrayList<>();
		runningTotal = new Integer[3];
		clearRunningTotal();
		playerBestScore = 0;
		setWinnerScore(0);
		setWinnerLogin(null);
	}
	
	public void nextPlayer() {
		gameTurn = (gameTurn%manyPlayers)+1;
		System.out.println("TURN NOW: "+gameTurn);
	}
	
	/**
	 * Sets the score as the running total after a turn has finished.
	 * 
	 * @param nscore		Score of this turn
	 * */
	public void addScore (int nscore) {
		if (nscore == 0) clearScore();
		else score = nscore;
			
	}
	
	public void addRunningTotal(int v1, int v2, int v3) {
		runningTotal[0] += v1;
		runningTotal[1] += v2;
		runningTotal[2] += v3;
		System.out.println("RUNNING TOTALS: "+runningTotal[0]+ " "+runningTotal[1]+ " "+runningTotal[2]);
	}
	
	
	
	public int getScore () {
		return this.score;
	}
	
	public void createWordContainer (String[] words, String[] hints) {
		wc = new WordContainer(words,hints);
	}

	public boolean parseResponse(Response res) {
		if (res.hasBestScore()) {
			
			playerBestScore = res.getBestScore();
			
		}
		if (res.hasPlayersRank()) {
			for (int i = 0; i < 3; i++) {
				rankNames.add(res.getPlayerName(i));
				rankScores.add(res.getPlayerScore(i));
			}
			
			playerBestScore = res.getBestScore();
			
		}
		if (res.hasWordsAndHints()) {
			
			String[] words = new String[4];
			String[] hints = new String[4];
			for (int i = 0; i < 4; i++) {
				words[i] = new String();
				hints[i] = new String();
				words[i] = res.getWord(i);
				hints[i] = res.getHint(i);
			}
			createWordContainer(words, hints);
			
		}
		if (res.hasPlayerTurn()) {
			
			playerTurn = res.getPlayerTurn();
			
		}
		if (res.hasPlayersLogin()) {
			
			for (int i = 0; i < manyPlayers; i++)
				playersLogin.add(res.getPlayerLogin(i));
			
		}
		return res.getOK();
	}
	
	public String getLogin () {
		return this.login;
	}

	public int getManyPlayers() {
		return manyPlayers;
	}

	public void setManyPlayers(int manyPlayers) {
		this.manyPlayers = manyPlayers;
	}
	
	public void nextState() {
		currentState = currentState.nextState();
	}
	
	public Integer getRunningTotal() {
		return runningTotal[getMyTurn()-1];
	}
	
	public void setPlayerLogin(String login) {
		this.login = login;
	}
	
	public int getMyTurn() {
		return this.playerTurn;
	}

	public String getWinnerLogin() {
		return winnerLogin;
	}

	public void setWinnerLogin(String winnerLogin) {
		this.winnerLogin = winnerLogin;
	}

	public int getWinnerScore() {
		return winnerScore;
	}
	
	public String getRankName (int ind) {
		return rankNames.get(ind);
	}
	
	public int getRankScore (int ind) {
		return rankScores.get(ind);
	}

	public void setWinnerScore(int winnerScore) {
		this.winnerScore = winnerScore;
	}
}
