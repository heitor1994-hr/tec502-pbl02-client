package model;

public class ScoreBoardItem implements Comparable<ScoreBoardItem> {
	private String playerLogin;
	private Integer total;
	
	public ScoreBoardItem(String login, Integer total) {
		setPlayerLogin(login);
		this.setTotal(total);
	}

	@Override
	public int compareTo(ScoreBoardItem o) {
		if (this.total > o.getTotal()) return 1;
		if (this.total < o.getTotal()) return -1;
		return 0;
	}

	public String getPlayerLogin() {
		return playerLogin;
	}

	public void setPlayerLogin(String playerLogin) {
		this.playerLogin = playerLogin;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}
}
