package model;

import java.io.IOException;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

import model.GameModel.RoomType;
import networking.GameConnection;
import networking.Response;

public class GameController {
	private GameModel gm = null;
	private Connector con = null;

	
	Object[] tryAgain = {"Tentar Novamente"};
	
	public GameController() {
		gm = GameModel.getInstance();
		try {
			con = new Connector();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void login (String name, String passwd) {
		boolean isOk = true;
	//	Response res = null;
		do {
			try {
				isOk = true;
				con.login(name, passwd);
				//isOk = res.getOK();
				//if (isOk) 
					gm.setPlayerLogin(name);
			} catch (UnknownHostException e) {
				isOk = false;
				e.printStackTrace();
				JOptionPane.showOptionDialog(null, "Host desconhecido.", 
						"ERRO", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, tryAgain, tryAgain[0]);
			} catch (IOException e) {
				isOk = false;
				e.printStackTrace();
				JOptionPane.showOptionDialog(null, "Conexão recusada.", 
						"ERRO", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, tryAgain, tryAgain[0]);
			}
		} while (!isOk);
		//return isOk;
	}
	
	public void register (String name, String passwd) {
		boolean isOk = true;
	//	Response res = null;
        do {
			try {
				isOk = true;
				con.register(name, passwd);
				//isOk = res.getOK();
				gm.setPlayerLogin(name);
			} catch (UnknownHostException e) {
				e.printStackTrace();
				isOk = false;
				JOptionPane.showOptionDialog(null, "Host desconhecido.", 
						"ERRO", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, tryAgain, tryAgain[0]);
			} catch (IOException e) {
				e.printStackTrace();
				isOk = false;
				JOptionPane.showOptionDialog(null, "Conexão recusada.", 
						"ERRO", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, tryAgain, tryAgain[0]);
			}
		} while (!isOk);
		//return isOk;
	}
	
	public void selectRoom(int option) {
		RoomType roomType = null;
	//	Response res = null;
		if (option == 0) {
			roomType = RoomType.TWO_PLAYERS;
			gm.setManyPlayers(2);
		}
		else if (option == 1) {
			roomType = RoomType.THREE_PLAYERS;
			gm.setManyPlayers(3);
		}
		
		boolean isOk = true;
		
		do {
			try {
				isOk = true;
				con.selectRoom(roomType, gm.getLogin());
			} catch (UnknownHostException e) {
				e.printStackTrace();
				isOk = false;
				JOptionPane.showOptionDialog(null, "Host desconhecido.", 
						"ERRO", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, tryAgain, tryAgain[0]);
			} catch (IOException e) {
				e.printStackTrace();
				isOk = false;
				JOptionPane.showOptionDialog(null, "Conexão recusada.", 
						"ERRO", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, tryAgain, tryAgain[0]);
			}
		} while (!isOk);
		
		//readResponse(res);
		//return res.isGameSet();
	}
	
	public void getBestPlayerRank (String login, int scoreRound4) {
		boolean isOk = true;
		do {
			try {
				isOk = true;
				con.getBestPlayersRank(login, scoreRound4);
			} catch (UnknownHostException e) {
				e.printStackTrace();
				isOk = false;
				JOptionPane.showOptionDialog(null, "Host desconhecido.", 
						"ERRO", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, tryAgain, tryAgain[0]);
			} catch (IOException e) {
				e.printStackTrace();
				isOk = false;
				JOptionPane.showOptionDialog(null, "Conexão recusada.", 
						"ERRO", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, tryAgain, tryAgain[0]);
			}
		} while (!isOk);
		
	}
	
	public void broadcastRouletteValue() {
		try {
			con.broadcastRouletteValue(gm.getMyTurn(), gm.getScore());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void broadcastLetter(char letter) {
		try {
			con.broadcastLetter(gm.getMyTurn(), letter);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void broadcastPass() {
		try {
			con.broadcastPass(gm.getMyTurn());
		} catch (IOException e) {
			e.printStackTrace();
		}
		gm.nextPlayer();
	}
	
	public void broadcastNextRound() {
		try {
			con.broadcastNextRound(gm.getMyTurn());
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void broadcastRank() {
		try {
			con.broadcastRank(gm.getRankName(0),gm.getRankName(1),gm.getRankName(2),gm.getRankScore(0),gm.getRankScore(1),gm.getRankScore(2));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getReceivedData() {
		return con.getReceivedData(gm.getMyTurn());
	}
	
	public boolean hasNewMessage() {
		return con.hasNewMessage();
	}
	
	public void addScore (int score) {
		gm.addScore(score);
	}
	
	public void reset() {
		gm.startGame();
	}

	public String jsExpression() {
		return gm.generateJSExpression();
	}
		
	public boolean readResponse () {
		Response res = con.getResponse();
		if (res.isGameSet()) {
			try {
				con.parseResponse(res);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		gm.parseResponse(res);
		return res.getOK();
	}

	public void nextState() {
		gm.nextState();
	}

	public boolean isReady() {
		return con.isReady();
	}

	public void addRunningTotal(int v1, int v2, int v3) {
		gm.addRunningTotal(v1, v2, v3);
	}
	
	public Integer getRunningTotal() {
		return gm.getRunningTotal();
	}

	public void getPlayersRank() {
		if (gm.getMyTurn() != 1) return;
		try {
			con.getBestPlayersRank(gm.getWinnerLogin(), gm.getWinnerScore());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void selectServer(String ip) {
		GameConnection.getInstance().setHost(ip);
	}
}
