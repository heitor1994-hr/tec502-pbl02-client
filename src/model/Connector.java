package model;

import java.io.IOException;
import java.net.UnknownHostException;

import model.GameModel.RoomType;
import networking.GameConnection;
import networking.Response;
import networking.p2pNetwork.BroadcastData;
import networking.p2pNetwork.LogicNetwork;

public class Connector {

	
	/** GameConnection reference for sending and receiving data through network*/
	private GameConnection gc = null;
	/** LogicNetwork Object for sending data to neighbors*/
	private LogicNetwork ln = null;
	
	public Connector () throws IOException {
		gc = GameConnection.getInstance();
		ln = new LogicNetwork();
	}

	
	public void register(String name, String password) throws UnknownHostException, IOException {		
		String request = gc.createRegisterRequest(name, password);
				
	    gc.sendString(request);
					
		//return response;
	}
	
	public void login(String name, String password) throws UnknownHostException, IOException {		
		String request = gc.createLoginRequest(name, password);
			
		gc.sendString(request);
	
	//	return response;
	}
	
	public void selectRoom(RoomType gameOption, String name)  throws UnknownHostException, IOException  {
			String request = "";
			if(gameOption == RoomType.TWO_PLAYERS){
				
				request = gc.join2PlayersGame(name);
				LogicNetwork.setAcks(1);
			} else if(gameOption == RoomType.THREE_PLAYERS){
				
				request = gc.join3PlayersGame(name);
				LogicNetwork.setAcks(2);
			}
				
			gc.sendString(request);
		}

	public void getBestPlayersRank (String name, int score) throws UnknownHostException, IOException {
		String req = gc.createRankRequest(name, score);
		
		gc.sendString(req);
	}
	
	public void broadcastRouletteValue (int player, int value) throws IOException {
		String req = ln.createRouletteBroadcastString(player, value);
		ln.sendData(req);
	}
	
	public void broadcastLetter (int player, char letter) throws IOException {
		String req = ln.createLetterBroadcastString(player, letter);
		ln.sendData(req);
	}
	
	public void broadcastPass (int player) throws IOException {
		String req = ln.createPassBroadcastString(player);
		ln.sendData(req);
	}
	
	public void broadcastNextRound(int player) throws IOException {
		String req = ln.createNextRoundBroadcast(player);
		ln.sendData(req);
	}
		
	public boolean hasNewMessage() {
		if (ln == null) return false;
		return ln.hasNewMessage();
	}
	
	public void setLogicNetwork(Response res) throws IOException {
		ln.startLogicNetwork(res.getMulticastAddr());
		ln.setPeerId(res.getPlayerTurn());
		ln.startReceiving();	
	}
	
	public String getReceivedData(int player) {
		BroadcastData bd = ln.getNextMessage();
		StringBuilder res = new StringBuilder();
		
		if (bd.getPlayerTurn() == player) return "NO_LISTENING";
		
		switch (bd.getType()) {
			case ROULETTE:
				res.append("ROULETTE ");
				res.append(bd.getRouletteValue());
				break;
			case PLAYED_LETTER:
				res.append("LETTER ");
				res.append(bd.getLetter());
				break;
			case PASS:
				res.append("PASS");
				break;
			case ACK:
				res.append("ACK");
				break;
			case NEXT_ROUND:
				res.append("NEXT_ROUND");
				break;
			case RANK:
				res.append("RANK "+bd.getRankName(0)+" "+bd.getRankScore(0)+" "+bd.getRankName(1)+" "+bd.getRankScore(1)+
						" "+bd.getRankName(2)+" "+bd.getRankScore(2));
				break;
		}
		
		return res.toString();	
	}
	
	public boolean isReady() {
		return gc.isReady();
	}


	public Response getResponse() {
		return gc.getLastResponse();
	}


	public void parseResponse(Response res) throws IOException {
		if (res.isGameSet()) {
			setLogicNetwork(res);
		}
	}


	public void broadcastRank(String n1, String n2, String n3, int s1, int s2, int s3) throws IOException {
		String req = ln.createRankBroadcast(n1, n2, n3, s1, s2, s3);
		ln.sendData(req);
	}
}
