package model;

public enum GameState {
	SELECT_SERVER(-3),
	LOGIN (-2),
	ROOM_SELECTION (-1),
	ROUND_1 (0),
	ROUND_2 (1),
	ROUND_3 (2),
	ROUND_4 (3),
	GAME_SCORE(4),
	GAME_END (5);
	
	private int stateVal;
		
	GameState(int state) {
		this.stateVal = state;
	}
	
	public int getVal() {
		return stateVal;
	}
	
	public boolean inGame() {
		if (stateVal == ROUND_1.getVal() || stateVal == ROUND_2.getVal() || stateVal == ROUND_3.getVal() 
				|| stateVal == ROUND_4.getVal()) {
			return true;
		}
		return false;
	}
	
	public GameState nextState() {
		if (this.equals(SELECT_SERVER)) return LOGIN;
		if (this.equals(LOGIN)) return ROOM_SELECTION;
		if (this.equals(ROOM_SELECTION)) return ROUND_1;
		if (this.equals(ROUND_1)) return ROUND_2;
		if (this.equals(ROUND_2)) return ROUND_3;
		if (this.equals(ROUND_3)) return ROUND_4;
		if (this.equals(ROUND_4)) return GAME_SCORE;
		if (this.equals(GAME_SCORE)) return GAME_END;
		return LOGIN;
	}
	
}
