
// the value one can obtain after spinning the roulette
var values = [
   {
    val: 1000
  	},
  	{
	  val: 50
    },
    {
    	val: 100
    },
    {
    	val: 0
    },
    {
    	val: 200
    },
    {
    	val: 300
    },
    {
    	val: 400
    },
    {
    	val: 500
    },
    {
    	val: 600
    },
    {
    	val: 700
    },
    {
    	val: 800
    },
    {
    	val: 900
    }
];

var $roulette;

function getPrice(value) {
	for (var i = 0; i < values.length; i++) {
		if (values[i].val === value) return i;
	}
	return 0;
}

function setRouletteTo(newValue) {	
	setValendo(newValue);

	$("#girar").off('click');
	
	var price = getPrice(newValue);
	
	$roulette.spin(Number(price)).done(function(){
		$('#valorValendo').val((newValue===0?"Perdeu Tudo":("R$ "+newValue+",00")));
		if (newValue == 0) clearAcumulado();
		app.addScore(newValue);
		setValendo(newValue);
		$("#girar").on('click', clickHandlerRoulette);
	}); 
}

var clickHandlerRoulette = function() {
	/*
	 * Function that handles when the spin button is clicked: it makes the
	 * roulette spin and randomly selects a value from it.
	 */
	$("#girar").off('click');
	$roulette.spin().done(function(value){
		$('#valorValendo').val((value.val===0?"Perdeu Tudo":("R$ "+value.val+",00")));
		app.addScore(value.val);
		setValendo(value.val);
		app.broadcastRouletteValue();
		$("#girar").on('click', clickHandlerRoulette);
		if (value.val != 0) stateSelecionarLetra();
		else{ 
			clearAcumulado();
			sendPass();
		}
	}); 

}

var setSpinButton = function() {
	$roulette =  $('.roulette').fortune(values);
	$("#girar").off('click');
	$("#girar").on('click', clickHandlerRoulette);
}


