/*var opts = {
  lines: 13 // The number of lines to draw
, length: 28 // The length of each line
, width: 14 // The line thickness
, radius: 42 // The radius of the inner circle
, scale: 1 // Scales overall size of the spinner
, corners: 1 // Corner roundness (0..1)
, color: '#000' // #rgb or #rrggbb or array of colors
, opacity: 0.25 // Opacity of the lines
, rotate: 0 // The rotation offset
, direction: 1 // 1: clockwise, -1: counterclockwise
, speed: 1 // Rounds per second
, trail: 60 // Afterglow percentage
, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
, zIndex: 2e9 // The z-index (defaults to 2000000000)
, className: 'spinner' // The CSS class to assign to the spinner
, top: '50%' // Top position relative to parent
, left: '50%' // Left position relative to parent
, shadow: false // Whether to render a shadow
, hwaccel: false // Whether to use hardware acceleration
, position: 'absolute' // Element positioning
}

var spinner;
var target;

var startSpinner = function() {
	target = document.getElementById('spinnerModal');
	spinner = new Spinner(opts);
}

var spinSpinner = function() {
	$("#spinnerModal").modal({
	    backdrop: 'static',
	    keyboard: false
	});
	$("#spinnerModal").modal("show");
	spinner.spin(target);
}

var stopSpinner = function() {
	spinner.stop();
	$("#spinnerModal").modal("hide");
}*/
(function($) {
  $.extend({
    spin: function(spin, opts) {

      if (opts === undefined) {
    	  var opts = {
    			  lines: 15 // The number of lines to draw
    			, length: 0 // The length of each line
    			, width: 14 // The line thickness
    			, radius: 51 // The radius of the inner circle
    			, scale: 1 // Scales overall size of the spinner
    			, corners: 1 // Corner roundness (0..1)
    			, color: '#000' // #rgb or #rrggbb or array of colors
    			, opacity: 0.25 // Opacity of the lines
    			, rotate: 0 // The rotation offset
    			, direction: 1 // 1: clockwise, -1: counterclockwise
    			, speed: 1.2 // Rounds per second
    			, trail: 60 // Afterglow percentage
    			, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    			, zIndex: 2e9 // The z-index (defaults to 2000000000)
    			, className: 'spinner' // The CSS class to assign to the spinner
    			, top: '50%' // Top position relative to parent
    			, left: '50%' // Left position relative to parent
    			, shadow: false // Whether to render a shadow
    			, hwaccel: false // Whether to use hardware acceleration
    			, position: 'absolute' // Element positioning
    			};
      }

      var data = $('body').data();

      if (data.spinner) {
        data.spinner.stop();
        delete data.spinner;
        $("#spinner_modal").remove();
        return this;
      }

      if (spin) {

        var spinElem = this;

        $('body').append('<div id="spinner_modal" style="background-color: rgba(0, 0, 0, 0.3); width:100%; height:100%; position:fixed; top:0px; left:0px; z-index:' + (opts.zIndex - 1) + '"/>');
        spinElem = $("#spinner_modal")[0];

        data.spinner = new Spinner($.extend({
          color: $('body').css('color')
        }, opts)).spin(spinElem);
      }

    }
  });
})(jQuery);