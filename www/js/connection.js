/**
 * 
 */
var gameTurn;
var myTurn;
var manyPlayers;

var setPlayerTurn = function (playerTurn) {
	myTurn = playerTurn;
}

var setGameTurn = function(turn) {
	gameTurn = turn;
}

var setName = function(player_n, name) {
	var id = "#player" + player_n + "Name";
	$(id).text(name);
}

var playersBoard = function(n_players, player){
	var divPlayerBoard = "";
	if (n_players == 2) {
		divPlayerBoard = divPlayerBoard.concat("<div id='player"+player+"' class='col-md-6 form-group has-success has-feedback' style='border: 1px solid black; text-align: center; margin-left: 250px; border-radius: 10px; box-shadow: 0px 0px 6px 2px #444;");
		divPlayerBoard = divPlayerBoard.concat(" padding-top: 10px; padding-bottom: 10px;'>");
		divPlayerBoard = divPlayerBoard.concat(" <label class='has-success' id='player"+player+"Name'>Name</label><br><hr>");
		divPlayerBoard = divPlayerBoard.concat(" <div class='col-sm-12 input-group'>");
		divPlayerBoard = divPlayerBoard.concat(" <span class='input-group-addon'> R$ </span> <input type='text' style='text-align:center;'");
		divPlayerBoard = divPlayerBoard.concat(" class='form-control input-info' id='valorAcumuladoPlayer"+player+"' value='0,00'");
		divPlayerBoard = divPlayerBoard.concat(" disabled></div></div> <br />");
	} else if (n_players == 3) {
		divPlayerBoard = divPlayerBoard.concat("<div id='player"+player+"' class='col-md-4 form-group has-success has-feedback' style='border: 1px solid black; text-align: center; margin-left: 250px; border-radius: 10px; box-shadow: 0px 0px 6px 2px #444;");
		divPlayerBoard = divPlayerBoard.concat(" padding-top: 10px; padding-bottom: 10px;'>");
		divPlayerBoard = divPlayerBoard.concat(" <label class='has-success' id='player" + player + "Name'>Name</label><br><hr>");
		divPlayerBoard = divPlayerBoard.concat(" <div class='col-sm-12 input-group' >");
		divPlayerBoard = divPlayerBoard.concat(" <span class='input-group-addon'> R$ </span> <input type='text' style='text-align:center;'");
		divPlayerBoard = divPlayerBoard.concat(" class='form-control input-info' id='valorAcumuladoPlayer"+player+"' value='0,00'");
		divPlayerBoard = divPlayerBoard.concat(" disabled></div></div>");
	}
	return divPlayerBoard;
}

var setScoreBoard = function(gameOption) {
	if (gameOption === 0) {
		manyPlayers = 2;
		$('#playerScores').append(playersBoard(2,1));
		$('#playerScores').append(playersBoard(2,2));
	} 
	else if (gameOption === 1) {
		manyPlayers = 3;
		$('#playerScores').append(playersBoard(3,1));
		$('#playerScores').append(playersBoard(3,2));
		$('#playerScores').append(playersBoard(3,3));
	}
	$("#player1").addClass("highlight");
}

var selectServer = function(serverIp) {
	app.selectServer(serverIp);

	var timeo = setTimeout(function () {
		app.nextState();
	}, 100);
}

var selectRoom = function(gameOption) {
	var points = '';
	manyPlayers = gameOption===0?2:3;
	var id = "#btn"+(manyPlayers)+"Players";
	$("#btn2Players").attr("disabled",true);
	$("#btn3Players").attr("disabled",true);
	var t = setInterval(function() {
		$(id).text("Procurando Jogadores"+points);
		points += '.';
		if (points == '....') points = '';
	}, 500);
	
	
	app.selectRoom(gameOption);
	
	var tt = setInterval(function() {
		if (app.isReady()) {
			app.readResponse();
			clearInterval(tt);
			clearInterval(t);
		}
	},2000);
}

var moveNextTurn = function () {
	$("#player"+gameTurn).removeClass("highlight");
	gameTurn = (gameTurn%manyPlayers)+1;
	$("#player"+gameTurn).addClass("highlight");
	if (gameTurn === myTurn) {
		stateGirarRoleta();
		$("#goBtn").attr("disabled",false);
	}
	else stateWaitingTurn();
	
}

function sendPass () {
	moveNextTurn();
	app.broadcastPass();
}

var waitResponse = function() {
	var interval = setInterval(function() {
		var hasMsg = app.hasNewMessage();
		if (hasMsg) { 
			var msg = app.getReceivedData();
			console.log(typeof(msg)+" "+msg);
			if (msg == "NO_MESSAGE" || msg == "NO_LISTENING") return;
			
			msg = msg.split(" ");
			if (msg[0] == "ROULETTE") {
				setRouletteTo(Number(msg[1]));
			}
			else if (msg[0] == "LETTER") {
				computeReceivedLetter(msg[1]);
			}
			else if (msg[0] == "PASS") {
				moveNextTurn();
			} 
			else if (msg[0] == "NEXT_ROUND") {
				goBtnAction();
			}
			else if (msg[0] == "RANK") {
				setPlayerRank(0,msg[1],Number(msg[2]));
				setPlayerRank(1,msg[3],Number(msg[4]));
				setPlayerRank(2,msg[5],Number(msg[6]));
				hideBestScore();
				setTotalScore(app.getRunningTotal());
				$("#advanceGame").attr("disabled",false);
			}
		}
		
		if (gameTurn != myTurn) {
			stateWaitingTurn();
		}
		
	}, 1000);
}

var isNumeric = function(num) {
	return !isNaN(num);
}
