var rodada;
var valendo = 0;
var acumulado = [0,0,0];
var finalizing = false;
/*
 * Method which defines the first state of the game: spin the roulette
 */

function getAcumulado () {
	return acumulado[gameTurn-1];
}

var setValendo = function (score) {
	valendo = score;
}

var setFinalizing = function(f) {
	finalizing = f;
}

var restartScore = function() {
	acumulado[0] = 0;
	acumulado[1] = 0;
	acumulado[2] = 0;
	$('#valorAcumuladoPlayer1').val("R$ "+acumulado[0]+",00");
	$('#valorAcumuladoPlayer2').val("R$ "+acumulado[1]+",00");
	if (manyPlayers > 2) $('#valorAcumuladoPlayer3').val("R$ "+acumulado[2]+",00");
	console.log("SCORE RESTARTED");
}

var clearAcumulado = function() {
	acumulado[gameTurn-1] = 0;
	$('#valorAcumulado').html("R$ "+acumulado[gameTurn-1]+",00");
	$('#valorAcumuladoPlayer'+gameTurn).val("R$ "+acumulado[gameTurn-1]+",00");
}

var stateWaitingTurn = function() {
	if (finalizing) return;
	$("#goBtn").attr("disabled",true);
	$("#toDo").html("Espere sua vez");
	$(".letterOption").attr("disabled",true);
	$("#girar").attr("disabled",true);
}

var stateGirarRoleta = function () {
	setSpinButton();
	$("#goBtn").attr("disabled",false);
	$("#toDo").html("Gire a roleta");
	$(".letterOption").attr("disabled",true);
	$("#girar").attr("disabled",false);
}

/*
 * Method which defines the state of selecting a letter
 * The chosen letter is disabled after selection
 */
var stateSelecionarLetra = function() {
	$("#goBtn").attr("disabled",true);
	$("#toDo").html("Selecione uma letra");
	$(".letterOption.btn-success").attr("disabled",false);
	$("#girar").attr("disabled",true);
}


//Called by the WebContainer to set the user's total score on the game view's modal
var setTotalScore = function(totalScore) {
	console.log("SET TOTAL SCORE: "+totalScore);
	$('#total_score').html("R$ " + totalScore+",00");
}

//Called by the WebContainer to set the word's hint on the game view's modal
var setHint = function (hint) {
	console.log("SET HINT");
	if (rodada == 1) $('.hint').text("DICA: ????");
	else $('.hint').text ("DICA: "+hint);
}

//Called by the WebContainer to set the ranking on the game view's modal
var setPlayerRank = function (ind, name, score) {
	console.log("SET PLAYER RANK: "+ind+" "+name+" "+score);
	if (name === "NO_PLAYER") name = "";
	switch (ind) {
		case 0: $("#first_place_name").html(name); $("#first_place_score").html("R$ "+score+",00"); break;
		case 1: $("#second_place_name").html(name); $("#second_place_score").html("R$ "+score+",00"); break;
		case 2: $("#third_place_name").html(name); $("#third_place_score").html("R$ "+score+",00"); break;
	}
}

var setRound = function(round) {
	rodada = round;
	$('#rodada').val(round);
}

// Called by the WebContainer to set the user's name on the game view
var setPlayerName = function(name) {
	$('#player_name').html(name);
}

//Called by the WebContainer to set the user's best score on the game view's modal
var setBestScore = function(score) {
	console.log("SET BEST SCORE: "+score);
	$("#best_score").html("R$ "+score+",00");
}

var hideBestScore = function() {
	$("#best_score_label").hide();
}

/*
 * Exhibits the modal with the ranking, best score and total score
 */
var showModal  = function() {
	console.log("SHOW MODAL");
	hideModals();
	$('#myModal').modal({
	    backdrop: 'static',
	    keyboard: false
	});
	
	$('#myModal').modal('show');
}

/*
 * Exhibits the modal where the user inserts their username and password
 */
var showIniModal = function(){
	hideModals();
	$("#iniModal").modal({
	    backdrop: 'static',
	    keyboard: false
	});
	$("#iniModal").modal('show');
	document.getElementById("name").focus();
}

var showServerModal = function(servers) {
    hideModals();
    $("#serverModal").modal({
        backdrop: 'static',
        keyboard: false
    });
    for (var i = 0; i < servers.length; i++) {
        var server = servers[i];
        $("#serverBody").append("<button id='server"+i+"' type='button' class='btn btn-primary btn-lg btn-block game-option' "+
            "onclick=\"selectServer('"+server.ip+"')\" data-loading-text='Connecting...'>"+server.name+" "+server.latency+" ms"+"</button>");
    }
    $("#serverModal").modal('show');
}

var showPartidaModal = function() {
	hideModals();
	$("#partidaModal").modal({
	    backdrop: 'static',
	    keyboard: false
	});
	if (myTurn == 1) {
		$("#advanceGame").attr("disabled",false);
	} else $("#advanceGame").attr("disabled",true);
	
	$("#partidaModal").modal('show');
}

var addToScoreBoard = function(player_n, login, total) {
	$("#"+player_n+"_place_game_name").html(login);
	$("#"+player_n+"_place_game_score").html("R$ "+total+",00");
}

var hideThirdPlace = function() {
	$("#3PlaceGame").hide();
}

var showRoomModal = function() {
	hideModals();
	$("#roomModal").modal({
	    backdrop: 'static',
	    keyboard: false
	});
	$("#roomModal").modal('show');
}

var hideModals = function() {
	$("#roomModal").modal('hide');
	$("#iniModal").modal('hide');
	$("#partidaModal").modal('hide');
	$("#myModal").modal('hide');
	$("#serverModal").modal('hide');
}

var advanceToEnd = function() {
	$("#best_score_label").hide();
	if (myTurn == 1) {
		app.getPlayersRank();
		
		$.spin("true");
		var advance = setInterval(function(){
			if (app.isReady()) {
				$.spin('false');
				app.readResponse();
				app.broadcastRank();
				clearInterval(advance);
			}
		}, 500); 
	} else {
		showModal();
	}
}

/*
 * Method to do the registration or login of a user
 * 
 * The parameter operation is responsible for defining which method should be trigged:
 * 		0 - login
 * 		1 - register
 * 
 * The login and registration is done if both username and password are no short than
 * 3 characters, no longer than 10 characters and are composed by alphanumeric characters
 * 
 * If everything is okay the login or register method is called from the JavaBridge class, which
 * returns a boolean value indicating whether the operation was successful or not
 */
var proceed = function(operation) {
	
	var name = $("#name").val();
	var pass = $("#password").val();
	console.log(name +" "+pass);

	if (name.length < 3 || pass.length < 3) {
		$('#status').html("Usuário e/ou senha muito pequena");
		if ($('#status_bar').prop('hidden')) $('#status_bar').attr('hidden', false);
		else $('#status_bar').fadeTo("fast", 0.5).fadeTo('fast', 1.0);
		return;
	}
	else if (name.length > 10 || pass.length > 10) {
		$('#status').html("Usuário e/ou senha muito grande");
		if ($('#status_bar').prop('hidden')) $('#status_bar').attr('hidden', false);
		else $('#status_bar').fadeTo("fast", 0.5).fadeTo('fast', 1.0);
		return;
	}
	else if (!name.match(/^[0-9A-Za-z]+$/) || !pass.match(/^[0-9A-Za-z]+$/)) {
		$('#status').html("Apenas caracteres alfanuméricos");
		if ($('#status_bar').prop('hidden')) $('#status_bar').attr('hidden', false);
		else $('#status_bar').fadeTo("fast", 0.5).fadeTo('fast', 1.0);
		return;
	}
	
	if(operation === 0){
		window.app.login(name, pass);
	}
	else if(operation === 1) {
		window.app.register(name, pass);
	}
	
	$.spin('true');
	var proceedInterval = setInterval(function(){
		if (window.app.isReady()) {
			$.spin('false');
            var isOkay = app.readResponse();
            if(!isOkay){
				$('#status').html("Usuário e/ou senha inválidos");
				if ($('#status_bar').prop('hidden')) $('#status_bar').attr('hidden', false);
				else $('#status_bar').fadeTo("fast", 0.5).fadeTo('fast', 1.0);
			}
			clearInterval(proceedInterval);
		}
	}, 100);
}

var goBtnAction = function(){
	$("#toDo").html("Finalizando Round...");
	$("#goBtn").attr("disabled",true);
	$("#girar").attr("disabled",true);
	setFinalizing(true);
	var timeout = setTimeout(function(){
		setFinalizing(false);
		app.addRunningTotal(acumulado[0],acumulado[1],acumulado[2]);
		restartScore();
		stateGirarRoleta();
		app.nextState();
	}, 3000);
}

var startRound = function () {
	stateGirarRoleta();
	$("#goBtn").click(function() {
		sendPass();
	});
	$("#reset_game").click(function() {
		app.reset();
	});
}






