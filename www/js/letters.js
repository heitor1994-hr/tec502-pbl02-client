/**
 *
 */

// array containing all letters of the alphabet
var letters = [
              	"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
              	 "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
              ];

var currentWord = "";

// this method dynamically inserts buttons corresponding to the letters on the game view
var setLetters = function() {
	$('.letterOption').remove();
	for(var i = 0; i < letters.length; i++){
		
		var letter = letters[i];
		$('#letters').append("<button class='btn btn-success letterOption'><strong>" + letter + "</strong></button>");
	}
}

/*
 * This method is called on the WebContainer class in order to set the word obtained from the server for the current level
 * 
 * A simple encryption method was defined to mask the value of each letter belonging to the word in case the game is run
 * in a browser. This encryption, however, is not necessarily needed anymore.
 */
var setCurrentWord = function (word) {
	currentWord = word.toUpperCase();
	setLetters();
	var wordLetters = currentWord.split("");
	$('.box').remove();
	for(var i = 0; i < wordLetters.length; i++){
		var letter = wordLetters[i];
		$('#wordLetters').append("<li class='box'><span class='letters'>"+encrypt(letter)+"</span></li>");
	}
	setControl();
	stateGirarRoleta();
	console.log(wordLetters);
}

/*
 * This method checks whether a word contains the selected letter or not
 * 
 * For each matched letter the total score is incremented with the value obtained
 * after spinning the roulette.
 * 
 * If there are still letters to be guessed, the next state is defined as "spin the roulette",
 * if not the next is "go to next level".
 */
var setControl = function () {
	$('.letterOption').each(function(index){
			$(this).click(function(){
				var letter = $(this).text();
				app.broadcastLetter(letter.charCodeAt());
				console.log(typeof(letter.charCodeAt()) +" "+letter);
				
				var found = checkLetter(letter, true);
				$(this).attr('disabled', true);
				$(this).removeClass('btn-success');
				$(this).addClass('btn-danger');
				stateGirarRoleta();	
				if (!found) {
					sendPass();
				}
			});
		});
}

var computeReceivedLetter = function(rcvLetter) {
	rcvLetter = rcvLetter.toUpperCase();
	var found = false;
	$('.letterOption').each(function(index){
		var letter = $(this).text();
		if (letter === rcvLetter) {
			found = checkLetter(letter, false);
			$(this).attr('disabled', true);
			$(this).removeClass('btn-success');
			$(this).addClass('btn-danger');
		}
	});
	return found;
}

function checkLetter(letter, me) {
	var letterIndex = [];
	var foundLetter = false;
	for (var i = 0; i < currentWord.length; i++) {
		if (currentWord[i] === letter) {
			letterIndex.push(i);
			foundLetter = true;
		}
	}
	var j = 0;
	$(".letters").each(function(i){
		if(i === letterIndex[j]){
			if (j === 0) {
				acumulado[gameTurn-1] += valendo;
				$('#valorAcumuladoPlayer'+gameTurn).val("R$ "+acumulado[gameTurn-1]+",00");
			}
			j++;
			var decryptedLetter = decrypt($(this).text());
			$(this).text(decryptedLetter);
			$(this).slideUp(300,function () {
			$(this).css("visibility","visible");
			$(this).slideDown(300);		
			if (completeWord()) {
				$("#girar").attr("disabled",true);
				if (me) {
					app.broadcastNextRound();
					goBtnAction();
					
				}
			}
			});
		}
	});		
	return foundLetter;
}



// Check whether a word has been completed or not
function completeWord () {
	var end = true;
	$(".letters").each(function(i){
		if ($(this).css("visibility") === 'hidden') {
			end = false;		
		}
	});	
	return end;
}

// It multiplies the ASCII code of a character by a constant in order to encrypt a letter
function encrypt(letter){
	
	var constant = 5494;
	
	return (letter.charCodeAt() * constant);
}

// It does the opposite of the encrypt method
function decrypt(value){
	
	var constant = 5494;
	
	var letterCode = value/constant;
	
	return String.fromCharCode(letterCode);
}