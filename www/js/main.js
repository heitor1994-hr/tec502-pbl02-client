/**
 * Sets up the page everytime it's loaded
 */

$(document).ready(function() {
	setSpinButton();
	startRound();
	waitResponse();
	startSpinner();
});