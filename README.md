User Stories
=======

Pré-Login
-----------
* ~~Como **usuário** desejo me registrar no sistema para poder logar~~
* ~~Como **usuário** desejo logar no sistema para poder jogar~~

Pós-Login, Pré-Jogo
-----------
* ~~Como **usuário** desejo selecionar se irei jogar numa sala de 2 ou 3 jogadores para iniciar uma partida~~
* (Opcional) Como **usuário** desejo voltar à partida da qual me desconectei para continuar jogando

Em Jogo
-----------
* ~~Como **usuário** devo girar a roleta para saber quanto vale a jogada ou se perco tudo~~
* ~~Como **usuário** devo selecionar uma letra para receber o valor tirado na roleta~~
* Como **usuário** posso passar a vez ao fim de minha jogada para não correr o risco de perder tudo
* Como **usuário** desejo visualizar as letras que já foram selecionadas por todos os jogadores   para que não sejam selecionadas novamente


Pós-Jogo
-----------
* Como **usuário** desejo visualizar o ranking da partida para saber quem é o vencedor
* Como **usuário** desejo visualizar meu melhor score já obtido para comparar com  meu rendimento na partida atual
* Como **usuário** desejo visualizar o ranking dos vencedores com melhores pontuações para saber  quem são e se estou entre eles
* Como **usuário** devo poder selecionar jogar novamente para voltar a sala de espera
* Como **usuário** devo poder selecionar finalizar o jogo para fechar o aplicativo


Requisitos de Sistema
=======

Pré-Login
-----------
* ~~O **sistema** deve se comunicar com o servidor para registrar um novo usuário~~
* ~~O **sistema** deve se comunicar com o servidor para autenticar um usuário existente~~

Pós-Login, Pré-Jogo
-----------
* ~~O **sistema** deve se comunicar com o servidor informando se o usuário deseja jogar em uma sala de 2 ou 3 jogadores~~
* (Opcional) O **sistema** deve comunicar aos vizinhos o login do usuário para identificação de cada jogador
* (Opcional) O **sistema** deve permitir ao usuário voltar ao jogo do qual se desconectou

Em Jogo
-----------
* ~~O **sistema** deve comunicar aos vizinhos o valor tirado na roleta~~
* ~~O **sistema** deve comunicar aos vizinhos a letra selecionada~~
* O **sistema** deve comunicar aos vizinhos se a vez foi passada

Pós-Jogo
-----------
* O **sistema** deve se comunicar com o servidor para enviar a pontuação e o login do vencedor
